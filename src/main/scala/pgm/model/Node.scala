package pgm.model

object Tex {
  val SEPARATOR = " & "
}


trait Elem {
  def +(e2: Elem) = AddOp(this, e2)

  def *(e2: Elem) = MulOp(this, e2)

  def tex: String
}

case class Cell(node: Node, value: Int, conditions: Seq[(Node, Int)]) extends Elem {
  override def tex =
    raw"""\text{${node.name.toLowerCase}}_${
      if (conditions.isEmpty) {
        value
      } else { raw"""{$value\_${conditions.map(_._2).mkString}}"""}
    }"""
}

case class AddOp(e1: Elem, e2: Elem) extends Elem {
  override def tex: String = s"${e1.tex} + ${e2.tex}"
}

case class MulOp(e1: Elem, e2: Elem) extends Elem {
  override def tex: String = s"${e1.tex} ${e2.tex}"
}

case class Node(name: String, size: Int, parents: Node*) {
  def tex: String = {
    def elements(f: Int => String) = (0 until size).map(f).mkString(" & ")

    val values = raw"""${elements(i => raw"\text{$name}_$i")}"""
    if (parents.nonEmpty) {
      val lines = Node.indexes(parents).map(is => {
        val conditions = is.zipWithIndex.map { case (i, index) => raw"""\text{${parents(index).name}}_$i""" }.mkString(Tex.SEPARATOR)
        val probs_index = is.mkString
        val probs = (0 until size).map(i =>raw"""\text{${name.toLowerCase}}_{$i\_$probs_index}""").mkString(Tex.SEPARATOR)
        conditions + Tex.SEPARATOR + probs + " \\\\"
      }).mkString("\n")

      raw"""
\begin{array}{${"c" * parents.length}|${"c" * size}}
${"& " * parents.length}$values \\
\hline
$lines
\end{array}
"""
    } else {
      raw"""\begin{array}{c|${"c" * size}}
& $values \\
\hline
& ${elements(i => raw"\text{${name.toLowerCase}}_{$i}")} \\
\end{array}
"""
    }
  }

  def elem(conditions: Map[Node, Int]) = Cell(this, conditions(this), this.parents.map(p => (p, conditions(p))))
}

case class Prob(probs: Seq[(Seq[(Node, Int)], Elem)]) {
  def tex = {
    val lines = probs.map(prob => {
      val conditions = prob._1.map(v => raw"""\text{${v._1.name}}_${v._2}""").mkString(Tex.SEPARATOR)
      val probs = prob._2.tex
      conditions + Tex.SEPARATOR + probs + "\\\\"
    }).mkString("\n")
    val size = probs(1)._1.length
    raw"""\begin{array}{${"c" * size}|c}
                                     |${"& " * size}\text{P}\\
                                     |\hline
                                     |$lines
                                     |\end{array}
                                     |""".stripMargin
  }

  def marginalize(nodes: Node*) = {
    val notFound = nodes.filterNot(probs.head._1.map(_._1).contains(_))
    assert(notFound.isEmpty,s"marginalization on nodes not found in the model is not supported: ${notFound.map(_.name).mkString}")
    val groups = probs
      .groupBy(prob => prob._1.filter(v => !nodes.contains(v._1)))
      .mapValues(_.map(_._2).reduceLeft((x1, x2) => x1 + x2))
    Prob(groups.keys.toList.sortBy(_.foldLeft(0)((acc, v) => v._2 + acc * 10)).map(v => (v, groups(v))))
  }
}

object Node {

  def indexes(nodes: Seq[Node]) = nodes.map(_.size).reverse.foldLeft(Seq(Nil: List[Int]))((l, size) => for (i <- 0 until size; ls <- l) yield i :: ls)

  def jointProb(nodes: Node*): Prob = Prob(indexes(nodes).map(nodes.zip(_)).map(v => (v, nodes.map(_.elem(v.toMap)).reduceLeft((e1: Elem, e2) => e1 * e2))))

}
