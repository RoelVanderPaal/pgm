package pgm.model

import org.scalatest.FlatSpec

class NodeSpec extends FlatSpec {
  val X = Node("X", 2)
  val Y = Node("Y", 2)
  val Z = Node("Z", 3)
  val A = Node("A", 2, X, Y)

  "A node Set" should "output unconditional size 2 tex" in {
    assert(Node("A", 2).tex ===
      """\begin{array}{c|cc}
        |& \text{A}_0 & \text{A}_1 \\
        |\hline
        |& \text{a}_{0} & \text{a}_{1} \\
        |\end{array}
        |""".stripMargin)
  }
  it should "output unconditional size 3 tex" in {
    assert(Node("B", 3).tex ===
      """\begin{array}{c|ccc}
        |& \text{B}_0 & \text{B}_1 & \text{B}_2 \\
        |\hline
        |& \text{b}_{0} & \text{b}_{1} & \text{b}_{2} \\
        |\end{array}
        |""".stripMargin)
  }
  it should "output conditional size 2 tex" in {
    assert(A.tex ===
      """
        |\begin{array}{cc|cc}
        |& & \text{A}_0 & \text{A}_1 \\
        |\hline
        |\text{X}_0 & \text{Y}_0 & \text{a}_{0\_00} & \text{a}_{1\_00} \\
        |\text{X}_0 & \text{Y}_1 & \text{a}_{0\_01} & \text{a}_{1\_01} \\
        |\text{X}_1 & \text{Y}_0 & \text{a}_{0\_10} & \text{a}_{1\_10} \\
        |\text{X}_1 & \text{Y}_1 & \text{a}_{0\_11} & \text{a}_{1\_11} \\
        |\end{array}
        |""".stripMargin)
  }
  it should "output jointProb" in {
    assert(Node.jointProb(X, Y, A).tex ===
      """\begin{array}{ccc|c}
        |& & & \text{P}\\
        |\hline
        |\text{X}_0 & \text{Y}_0 & \text{A}_0 & \text{x}_0 \text{y}_0 \text{a}_{0\_00}\\
        |\text{X}_0 & \text{Y}_0 & \text{A}_1 & \text{x}_0 \text{y}_0 \text{a}_{1\_00}\\
        |\text{X}_0 & \text{Y}_1 & \text{A}_0 & \text{x}_0 \text{y}_1 \text{a}_{0\_01}\\
        |\text{X}_0 & \text{Y}_1 & \text{A}_1 & \text{x}_0 \text{y}_1 \text{a}_{1\_01}\\
        |\text{X}_1 & \text{Y}_0 & \text{A}_0 & \text{x}_1 \text{y}_0 \text{a}_{0\_10}\\
        |\text{X}_1 & \text{Y}_0 & \text{A}_1 & \text{x}_1 \text{y}_0 \text{a}_{1\_10}\\
        |\text{X}_1 & \text{Y}_1 & \text{A}_0 & \text{x}_1 \text{y}_1 \text{a}_{0\_11}\\
        |\text{X}_1 & \text{Y}_1 & \text{A}_1 & \text{x}_1 \text{y}_1 \text{a}_{1\_11}\\
        |\end{array}
        |""".stripMargin)
  }
  it should "marginalize" in {
    val XYA = Node.jointProb(X, Y, A)
    val XY = XYA.marginalize(A)
    //    println(XY.tex)
  }
  it should "marginalize 2" in {
    val A = Node("A", 2)
    val B = Node("B", 2, A)
    val C = Node("C", 2, B)
    val ABC = Node.jointProb(A, B, C)
//    println(ABC.tex)
//    println(ABC.marginalize(A, B).tex)
  }
  it should "marginalize 3" in {
    val A = Node("A", 2)
    val B = Node("B", 2, A)
    val C = Node("C", 2, B)
    val AB = Node.jointProb(A, B)
    println(AB.tex)
    println(AB.marginalize(A).tex)
    println(AB.marginalize(B).tex)
  }
}